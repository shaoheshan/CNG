ARG BUILD_IMAGE=

FROM ${BUILD_IMAGE}

ARG GITALY_SERVER_VERSION=master
ARG GITALY_GIT_REPO_URL
ARG NAMESPACE=gitlab-org
ARG PROJECT=gitaly
ARG API_URL=
ARG API_TOKEN=
ARG FIPS_MODE=

ARG BUNDLE_OPTIONS="--jobs 4"

ADD gitlab-ruby.tar.gz /
ADD gitlab-go.tar.gz /

ENV LANG=C.UTF-8
ENV PRIVATE_TOKEN=${API_TOKEN}
ENV LIBDIR ${LIBDIR:-"/usr/lib64"}

COPY shared/build-scripts/ /build-scripts

RUN mkdir /assets \
    && ln -sf /usr/local/go/bin/* /usr/local/bin \
    && /gitlab-fetch \
        "${API_URL}" \
        "${NAMESPACE}" \
        "${PROJECT}" \
        "${GITALY_SERVER_VERSION}" \
    && cd ${PROJECT}-${GITALY_SERVER_VERSION}/ruby \
    && bundle config set --local deployment 'true' \
    && bundle config set --local without 'development test' \
    && bundle install ${BUNDLE_OPTIONS} \
    && FIPS_MODE=${FIPS_MODE} /build-scripts/reinstall-grpc-if-fips /build-scripts/patches \
    && cd .. \
    && cp -R ./ruby /srv/gitaly-ruby \
    && install -D LICENSE /licenses/GitLab.txt \
    && rm -rf /srv/gitaly-ruby/spec /srv/gitaly-ruby/features \
    && touch .ruby-bundle \
    && if [ -n "${GITALY_GIT_REPO_URL}" ]; then export GIT_REPO_URL="${GITALY_GIT_REPO_URL}" ; fi \
    && FIPS_MODE=${FIPS_MODE} make install WITH_BUNDLED_GIT=YesPlease \
    && /build-scripts/cleanup-gems /srv/gitaly-ruby/vendor/bundle/ruby \
    && cp -R --parents \
      /usr/local/bin/gitaly* \
      /usr/local/bin/praefect \
      ${LIBDIR}/ruby/gems/ \
      /srv/gitaly-ruby \
      /licenses \
      /assets
